import 'package:flutter/material.dart';
import 'package:kabisaquote/apiclient/apiclient.dart';
import 'package:kabisaquote/apiclient/jsonclient.dart';
import 'package:kabisaquote/apiclient/quotemodel.dart';
import 'package:kabisaquote/pages/home/components/c_quotecard.dart';
import 'package:kabisaquote/utils/helper_development.dart';
import 'package:kabisaquote/utils/helpers_navigation.dart';
import 'package:shake/shake.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late PageController _pageController;
  late Future<StormQuoteModel> _myQuote;
  late ShakeDetector detector;

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  Future<StormQuoteModel> getQuote() async {
    //  Here catching the error so the FutureBuilder can do something with it and Stop showing the Circle.
    printGreen("Getting random quote data");
    return getRandomStormQuote().catchError((err) {
      logGreenInv("I have error $err");
      return getJsonQuote();
      // return Future<StormQuoteModel>.error(err);
    });
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    _myQuote = getQuote();

    detector = ShakeDetector.waitForStart(onPhoneShake: () {
      printBlue("Shaking");
      setState(() {
        _myQuote = getQuote();
      });
    });
    // _myQuote = getJsonQuote();

    detector.startListening();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Navigator.canPop(context)
          ? () async => true
          : () async {
              bool? result = await areYouSureExitDialog(
                context,
                title: 'Weet je het zeker?',
                content: 'Wil je de App afsluiten?',
                yesValue: 'Ja',
                noValue: 'Nee',
              );
              result ??= false;
              return result;
            },
      child: Scaffold(
        appBar: AppBar(title: const Center(child: Text("Shake or Swipe"))),
        body: SafeArea(
          child: FutureBuilder<StormQuoteModel>(
            future: _myQuote,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                StormQuoteModel aQuote = snapshot.data;
                return PageView.builder(
                  controller: _pageController,
                  // itemCount: 3,
                  padEnds: false,
                  allowImplicitScrolling: true,
                  onPageChanged: (index) {
                    setState(() {
                      _myQuote = getQuote();
                      // _myQuote = getJsonQuote();
                    });
                  },
                  itemBuilder: ((context, index) {
                    return Center(
                      child: QuoteCard(
                        quote: aQuote.quote,
                        author: aQuote.author,
                      ),
                    );
                  }),
                );
              } else if (snapshot.hasError) {
                return Center(
                    child: Column(
                  children: [
                    const Text("NO DATA"),
                    IconButton(
                        onPressed: () => Navigator.of(context).pop(),
                        icon: const Icon(Icons.close_rounded))
                  ],
                ));
              } else {
                return const Center(
                  child: SizedBox(
                    height: 100,
                    width: 100,
                    child: CircularProgressIndicator(
                      strokeWidth: 5.0,
                      backgroundColor: Colors.grey,
                    ),
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
