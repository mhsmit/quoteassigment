import 'package:flutter/material.dart';

class QuoteCard extends StatelessWidget {
  const QuoteCard({
    Key? key,
    required this.quote,
    required this.author,
  }) : super(key: key);
  final String quote;
  final String author;
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(8),
        child: Card(
          margin: const EdgeInsets.all(50),
          elevation: 10,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Flexible(
                    flex: 1,
                    child: Icon(
                      Icons.format_quote_rounded,
                      size: 42,
                      color: Colors.grey,
                    ),
                  ),
                  Flexible(flex: 3, child: Text(quote)),
                  const Flexible(
                    flex: 1,
                    child: Icon(
                      Icons.format_quote_rounded,
                      size: 42,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(author),
                  const SizedBox(
                    width: 20,
                  )
                ],
              )
            ],
          ),
        ));
  }
}
