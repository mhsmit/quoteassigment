import 'package:flutter/material.dart';
import 'package:kabisaquote/pages/home/home.dart';
import 'package:kabisaquote/routes/routing.dart';
import 'package:kabisaquote/utils/helper_development.dart';
import 'package:kabisaquote/utils/helpers_media_size.dart';

// NavigatorKey is used for providing a context for showing dialogboxes when there is no context yet (like in a Provider)
final navigatorKey = GlobalKey<NavigatorState>();
void main() => runApp(
      const QuoteApp(),
    );

class QuoteApp extends StatelessWidget {
  const QuoteApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    printGreenInv("Starting the Application");
    return MaterialApp(
      builder: (BuildContext context, Widget? child) {
        /// [appTextFactor] is used for small devices. It sets the scale of the font.
        return MediaQuery(
          data: appTextFactor(context: context),
          child: child!,
        );
      },
      title: 'Kabisa Quote',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      initialRoute: '/',
      onGenerateRoute: KabisaQuoteRouter.generateRoute,
      navigatorKey: navigatorKey,
      home: const Home(),
    );
  }
}
