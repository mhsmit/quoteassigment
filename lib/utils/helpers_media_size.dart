import 'package:flutter/material.dart';
import 'package:kabisaquote/utils/helper_development.dart';

/// This helper calculates the [appTextFactor] for Small devices. It returns
/// MediaQueryData that has to be called in the main builder function.
/// mhs 2022
MediaQueryData appTextFactor({required BuildContext context}) {
  double myScale;
  double thisAppWidth = appWidth(context: context);
  if (thisAppWidth < 400) {
    myScale = thisAppWidth / 450;
  } else {
    myScale = 1;
  }
  printRed('AppTextFactor = $myScale');
  return MediaQuery.of(context).copyWith(textScaleFactor: myScale);
}

double appWidth({required BuildContext context}) {
  return MediaQuery.of(context).size.width;
}

double appHeight({required BuildContext context}) {
  return MediaQuery.of(context).size.height;
}
