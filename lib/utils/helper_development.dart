import 'dart:developer' as developer;

/// [log] Blue text
void logBlue(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  developer.log('\x1B[34m$theTime$msg\x1B[0m');
}

/// [log] Green text
void logGreen(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  developer.log('\x1B[32m$theTime$msg\x1B[0m');
}

/// [log] Yellow text
void logYellow(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  developer.log('\x1B[33m$theTime$msg\x1B[0m');
}

/// [log] Red text
void logRed(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  developer.log('\x1B[31m$theTime$msg\x1B[0m');
}

/// [log] Green back ground Black text
void logGreenInv(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  developer.log('\x1B[1;30;42m$theTime$msg\x1B[0m ');
}

/// [log] Purple text
void logPurple(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  developer.log('\x1B[95m$theTime$msg\x1B[0m ');
}

/// [log] Purple text
void logCyan(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  developer.log('\x1B[96m$theTime$msg\x1B[0m ');
}

/// [print] Blue text
void printBlue(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  print('\x1B[34m$theTime$msg\x1B[0m');
}

/// [print] Green text
void printGreen(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  print('\x1B[32m$theTime$msg\x1B[0m');
}

/// [print] Green background Black text
void printGreenInv(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  print('\x1B[1;30;42m$theTime$msg\x1B[0m ');
}

/// [print] Yellow text
void printYellow(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  print('\x1B[33m$theTime$msg\x1B[0m');
}

/// [print] Red text
void printRed(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  print('\x1B[31m$theTime$msg\x1B[0m ');
}

/// [print] Red background white text
void printRedInv(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  print('\x1B[1;37;41m$theTime$msg\x1B[0m ');
}

/// [print] Purple text
void printPurple(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  print('\x1B[95m$theTime$msg\x1B[0m ');
}

/// [print] Purple text
void printCyan(String msg, {bool time = false}) {
  String theTime = time == true ? DateTime.now().toString() + " " : "";
  print('\x1B[96m$theTime$msg\x1B[0m ');
}
