import 'package:flutter/material.dart';

/// This shows a dialog to exit the App. [areYouSureExitDialog] can have optional variables like title, content and values for the yes no buttons.
Future<bool?> areYouSureExitDialog(
  BuildContext context, {
  String title = "Are you sure?",
  String content = 'Do you want to close the App?',
  String yesValue = 'yes',
  String noValue = 'no',
}) {
  return showDialog<bool>(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: <Widget>[
        ElevatedButton(
          style: const ButtonStyle(),
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(yesValue),
        ),
        ElevatedButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: Text(noValue),
        ),
      ],
    ),
  );
}
