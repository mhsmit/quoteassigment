import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:kabisaquote/apiclient/quotemodel.dart';
import 'package:kabisaquote/utils/helper_development.dart';

const apiHostStorm = 'http://quotes.stormconsultancy.co.uk/';
const baseUrlStorm = apiHostStorm;

Future<StormQuoteModel> getRandomStormQuote() async {
  try {
    var url = Uri.parse('$baseUrlStorm/random.json');
    var response =
        await http.get(url, headers: {'content-type': 'application/json'});
    if (response.statusCode == 200) {
      return StormQuoteModel.fromJson(
          jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      throw Exception('GET StormQuote ERROR');
    }
  } catch (e) {
    logRed("Have error $e");
    return Future<StormQuoteModel>.error(e);
  }
}
