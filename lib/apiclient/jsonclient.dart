import 'dart:math';

import 'package:flutter/services.dart';
import 'package:kabisaquote/apiclient/quotemodel.dart';
import 'package:kabisaquote/utils/helper_development.dart';

Future<StormQuoteModel> getJsonQuote() async {
  Future<StormQuoteModel> quotes;
  printRed('Fetching Json data');
  // simulate fetching using a file so added this delay
  // await Future.delayed(const Duration(milliseconds: 500));
  quotes = rootBundle
      .loadString('assets/json/quotes.json', cache: false)
      .then((jsonString) {
    List<StormQuoteModel> quoteList = quoteStormListModelFromJson(jsonString);
    Random random = Random();
    int rndQuoteNumber = random.nextInt(quoteList.length);
    return quoteList[rndQuoteNumber];
    // return stormQuoteModelFromJson(jsonString);
  }).onError(
    (error, stackTrace) {
      printRedInv("Have error $error");
      return Future<StormQuoteModel>.error(error!);
    },
  );
  return quotes;
}
