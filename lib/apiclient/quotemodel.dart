import 'dart:convert';

/// Random You can get a random quote by requesting:
///   GET http://quotes.stormconsultancy.co.uk/random.json

StormQuoteModel stormQuoteModelFromJson(String str) =>
    StormQuoteModel.fromJson(json.decode(str));
String stormQuoteModelToJson(StormQuoteModel data) =>
    json.encode(data.toJson());

List<StormQuoteModel> quoteStormListModelFromJson(String str) =>
    List<StormQuoteModel>.from(
        json.decode(str).map((x) => StormQuoteModel.fromJson(x)));
String quoteStormListModelToJson(List<StormQuoteModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class StormQuoteModel {
  StormQuoteModel({
    required this.id,
    required this.author,
    required this.quote,
    required this.permalink,
  });

  final int id;
  final String author;
  final String quote;
  final String permalink;

  factory StormQuoteModel.fromJson(Map<String, dynamic> json) =>
      StormQuoteModel(
        id: json["id"],
        author: json["author"],
        quote: json["quote"],
        permalink: json["permalink"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "author": author,
        "quote": quote,
        "permalink": permalink,
      };
}

/// Random quote https://quote-garden.herokuapp.com/api/v3/quotes/random
QuoteGardenModel quoteGardenModelFromJson(String str) =>
    QuoteGardenModel.fromJson(json.decode(str));

String quoteGardenModelToJson(QuoteGardenModel data) =>
    json.encode(data.toJson());

class QuoteGardenModel {
  QuoteGardenModel({
    required this.statusCode,
    required this.message,
    required this.pagination,
    required this.totalQuotes,
    required this.data,
  });

  final int statusCode;
  final String message;
  final Pagination pagination;
  final int totalQuotes;
  final List<Datum> data;

  factory QuoteGardenModel.fromJson(Map<String, dynamic> json) =>
      QuoteGardenModel(
        statusCode: json["statusCode"],
        message: json["message"],
        pagination: Pagination.fromJson(json["pagination"]),
        totalQuotes: json["totalQuotes"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "message": message,
        "pagination": pagination.toJson(),
        "totalQuotes": totalQuotes,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    required this.id,
    required this.quoteText,
    required this.quoteAuthor,
    required this.quoteGenre,
    required this.v,
  });

  final String id;
  final String quoteText;
  final String quoteAuthor;
  final String quoteGenre;
  final int v;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["_id"],
        quoteText: json["quoteText"],
        quoteAuthor: json["quoteAuthor"],
        quoteGenre: json["quoteGenre"],
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "quoteText": quoteText,
        "quoteAuthor": quoteAuthor,
        "quoteGenre": quoteGenre,
        "__v": v,
      };
}

class Pagination {
  Pagination({
    required this.currentPage,
    required this.nextPage,
    required this.totalPages,
  });

  final int currentPage;
  final dynamic nextPage;
  final int totalPages;

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
        currentPage: json["currentPage"],
        nextPage: json["nextPage"],
        totalPages: json["totalPages"],
      );

  Map<String, dynamic> toJson() => {
        "currentPage": currentPage,
        "nextPage": nextPage,
        "totalPages": totalPages,
      };
}
