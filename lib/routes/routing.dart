import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kabisaquote/pages/home/home.dart';
import 'package:kabisaquote/routes/route_constants.dart';

class KabisaQuoteRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case startRoute:
        return CupertinoPageRoute(builder: (_) => const Home());
      case homeRoute:
        return CupertinoPageRoute(builder: (_) => const Home());
      default:
        return CupertinoPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
      // case questionsRoute:
      //   final QuestionListArguments args =
      //       settings.arguments as QuestionListArguments;
      //   return CupertinoPageRoute(
      //       builder: (_) => QuestionsMain(arguments: args));
    }
  }
}
