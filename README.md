# kabisaquote

An Kabisa assignment

## The App

Your assignment is to build a cross platform Flutter application for iOS and
Android. The main screen of your application should display a random
quote retrieved from a web API and provide some means to display the
next random quote. The application should also incorporate at least one
native device feature like GPS, accelerometer, native sharing, etc and
handle a lack of network connectivity gracefully. All other requirements are
up to you! You can be as creative as you like.

When you shake the app It will change the Quote as well. 